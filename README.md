# **todo** Project

Proyecto para gestionar una lista de tareas diseañado para prácticar la tecnología Docker en un escenario real.

## Entorno de desarrollo

Arrancamos los servicios:

```bash
docker compose up -d
```

Ahora necesitamos crear el fichero `.env` para el proyecto `todo-api`. Para ello, copia el contenido del fichero `.env.example` en un nuevo fichero `.env` y comprueba que las variabes de base de datos sean correctas:

```bash
DB_CONNECTION=mysql
DB_HOST=mariadb
DB_PORT=3306
DB_DATABASE=todo
DB_USERNAME=root
DB_PASSWORD=secret
```

Ahora necesitamos genera los datos de prueba. Para ello, nos conectaremos al contendor `todolist-api` y ejecutaremos `php artisan migrate:fresh --seed"

```bash
docker exec -it todolist-api-1 sh
php artisan migrate:fresh --seed
```